import { configureStore, createSlice } from "@reduxjs/toolkit";

export const marathonSlice = createSlice({
  name: "marathons",
  initialState: {
    marathon: undefined,
    open: false,
    token: undefined,
  },
  reducers: {
    selectMarathon: (state, action) => {
      state.marathon = action.payload;
    },
    setOpen: (state, action) => {
      state.open = action.payload;
    },
    setToken: (state, action) => {
      state.token = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { selectMarathon, setOpen } = marathonSlice.actions;

export const store = configureStore({
  reducer: { marathons: marathonSlice.reducer },
});

export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
