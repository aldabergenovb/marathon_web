import { ConfigProvider } from "antd";
import ru_RU from "antd/locale/ru_RU";
import "antd/dist/reset.css";
import "./App.scss";
import Main from "./main/Main";
import { useAppSelector } from "./store/hooks";
import { Routes, Route } from "react-router-dom";
import AdminPanel from "./components/AdminPanel/AdminPanel";
import LoginPage from "./components/LoginPage/LoginPage";

function App() {
  const theme = useAppSelector((state) => state?.marathons?.marathon?.style);

  return (
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: theme ? theme.color : "#1450a3",
          colorBgLayout: theme ? theme.color : "#1450a3",
        },
      }}
      locale={ru_RU}
    >
      <Routes>
        <Route path="/admin" element={<AdminPanel />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/" element={<Main />} />
        <Route path="*" element={<Main />} />
      </Routes>
    </ConfigProvider>
  );
}

export default App;
