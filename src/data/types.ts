export type MarathonsType = {
  key: any;
  title: string;
  subTitle: string;
  about?: string;
  date: Date;
  dateText: string;
  image: string;
  races?: RacesType[];
  active: boolean;
  style: {
    color: string;
  };
};

export type RacesType = {
  key: any;
  distance: string;
  duration: string;
  age: string;
  price: number;
  image: string;
};
