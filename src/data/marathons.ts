import { MarathonsType } from "./types";

export const marathons: MarathonsType[] = [
  {
    key: "1",
    title: "FOREST RUN",
    subTitle: "Регистрация на FOREST RUN-2023 в Боровом ЗАВЕРШЕНА. Желаем всем участникам удачи!",
    date: new Date("10.22.2023"),
    dateText: "22 октября 2023 г.",
    active: true,
    image: "boro.jpg",
    races: [
      {
        key: "1",
        distance: "500м Kids Run",
        duration: "По последнему участнику",
        price: 3000,
        age: "от 8 до 12 лет",
        image: "src/assets/500m.jpg"
      },
      {
        key: "2",
        distance: "5км",
        duration: "1 час",
        price: 8000,
        age: "от 18 лет",
        image: "src/assets/5km.jpg"
      },
      {
        key: "3",
        distance: "10км",
        duration: "2 часа",
        price: 10000,
        age: "от 18 лет",
        image: "src/assets/10km.jpg"
      },
    ],
    style: {
      color: "#09bd00",
    },
  },
  {
    key: "2",
    title: "ISHIM HALF MARATHON",
    subTitle: "Город Астана",
    date: new Date("03.31.2024"),
    dateText: "31 марта 2024 г.",
    active: false,
    image: "koksh.jpeg",
    style: {
      color: "#d4cd00",
    },
  },
];
