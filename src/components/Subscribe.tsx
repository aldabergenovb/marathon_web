import { Button, Input } from "antd";
import Container from "./Container";
import { isMobile } from "react-device-detect";

const Subscribe = () => {
  return (
    <Container
      background={{
        backgroundColor: "#236bff",
        paddingTop: "50px",
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundAttachment: "fixed",
        padding: isMobile ? "40px 20px" : 90,
        // backgroundImage:
        //   "linear-gradient(180deg,rgba(0,0,0,0.7) 0%,rgba(0,0,0,0) 100%),url(https://images.pexels.com/photos/3601094/pexels-photo-3601094.jpeg?auto=compress&cs=tinysrgb&w=1600)",
      }}
    >
      <div style={{ fontSize: isMobile ? "1.3rem" : "1.9rem", color: "#fff" }}>
        Подпишись что бы получать новости от нас
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: isMobile ? "column" : "row",
          marginTop: "20px",
        }}
        className={`subscribe ${isMobile ? "isMobile" : ""}`}
      >
        <Input className="subscribe-input" bordered={false} />
        <Button size="large" type="primary" className="subscribe-button">
          Подпишись
        </Button>
      </div>
    </Container>
  );
};

export default Subscribe;
