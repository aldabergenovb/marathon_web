import { Col, Row } from "antd";
import DistanceCard from "./DistanceCard";
import { marathons } from "../data/marathons";

const MarathonsList = () => {
  return (
    <div style={{ padding: "2rem", display: "flex", justifyContent: "center" }}>
      <Row justify="center" gutter={[16, 16]} style={{ width: "1280px" }}>
        {marathons.map((marathon) => (
          <Col key={marathon.key} xs={24} sm={12} md={12} lg={12}>
            <DistanceCard marathon={marathon} />
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default MarathonsList;
