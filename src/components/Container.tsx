import { Carousel } from "antd";
import { marathons } from "../data/marathons";
import { CarouselRef } from "antd/es/carousel";
import { useRef, useEffect } from "react";
import { useAppSelector } from "../store/hooks";
import React from "react";
import { MarathonsType } from "../data/types";

const Container = ({
  children,
  background,
  isStatic = false,
  ...props
}: {
  children: React.ReactNode;
  isStatic?: boolean;
  background?: any;
}) => {
  const data = useAppSelector((state) => state.marathons.marathon);
  const carouselRef = useRef<CarouselRef>(null);

  useEffect(() => {
    if (data) {
      carouselRef.current?.goTo(marathons?.findIndex((m) => m === data));
    } else {
      carouselRef.current?.goTo(2);
    }
  }, [data]);

  const updatedChildren = (marathon: MarathonsType) =>
    React.Children.map(children, (child: any) => {
      return React.cloneElement(child, { marathon: marathon });
    });

  if (isStatic)
    return (
      <div {...background}>
        <div
          style={{
            maxWidth: "1280px",
            margin: "auto",
          }}
          {...props}
        >
          {children}
        </div>
      </div>
    );

  return (
    <div {...background}>
      <div
        style={{
          maxWidth: "1280px",
          margin: "auto",
        }}
        {...props}
      >
        <Carousel
          ref={carouselRef}
          dots={false}
          effect="fade"
          style={{ height: "100%" }}
        >
          {marathons.map((marathon, index) => (
            <React.Fragment key={index + 1}>
              {updatedChildren(marathon)}
            </React.Fragment>
          ))}
        </Carousel>
      </div>
    </div>
  );
};

export default Container;
