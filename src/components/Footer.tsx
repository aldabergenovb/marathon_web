import {
  InstagramOutlined,
  MailOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import { Space, theme } from "antd";
import { isMobile } from "react-device-detect";

const Footer = () => {
  const { token } = theme.useToken();

  return (
    <div
      style={{
        backgroundColor: token.colorBgLayout,
        display: "flex",
        padding: "15px 0",
        color: "#fff",
      }}
    >
      <div
        style={{
          width: "1280px",
          height: isMobile ? 150 : "auto",
          padding: isMobile ? 15 : "20px 0",
          margin: "auto",
          display: "flex",
          flexDirection: isMobile ? "column" : "row",
          justifyContent: "space-between",
        }}
      >
        <Space
          onClick={() =>
            window.open(
              "https://www.instagram.com/aqmola_marathon/?igshid=MzRlODBiNWFlZA%3D%3D",
              "_blank"
            )
          }
        >
          <InstagramOutlined />
          aqmola_marathon
        </Space>
        <Space>
          <MailOutlined />
          aqmolamarathon@gmail.com
        </Space>
        <Space onClick={() => (window.location.href = `tel:+7474555206`)}>
          <PhoneOutlined />
          +77474555206
        </Space>
        <Space onClick={() => (window.location.href = `tel:+77028787878`)}>
          <PhoneOutlined />
          +77028787878
        </Space>
        <Space onClick={() => (window.location.href = `tel:+77052801440`)}>
          <PhoneOutlined />
          +77052801440
        </Space>
      </div>
    </div>
  );
};

export default Footer;
