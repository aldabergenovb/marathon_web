import { Button, Card, Typography } from "antd";
import { MarathonsType } from "../data/types";
import { useAppDispatch } from "../store/hooks";
import { selectMarathon } from "../store/store";

const { Title } = Typography;

const DistanceCard = ({ marathon }: { marathon: MarathonsType }) => {
  const dispatch = useAppDispatch();

  const handleButtonClick = () => {
    dispatch(selectMarathon(marathon));
    // Scroll to the top of the page when the button is clicked
  };

  return (
    <Card
      className="distance_card"
      hoverable
      style={{
        height: "100%",
        color: "#fff",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          height: "100%",
        }}
      >
        <Title className="card_title" level={4}>
          "{marathon.title}" <br /> {marathon.subTitle} <br />{" "}
          {marathon.dateText}
        </Title>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Button
            className="card_button"
            size="large"
            onClick={handleButtonClick}
          >
            Подробнее
          </Button>
        </div>
        {marathon.races && (
          // <div className="card_duration">{marathon.races[0].distance}</div>
          <div className="card_duration">
            {marathon.races.map((el) => el.distance).join(" | ")}
          </div>
        )}
      </div>
    </Card>
  );
};

export default DistanceCard;
