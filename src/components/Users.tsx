import { TeamOutlined } from "@ant-design/icons";
import { Button, Col, Row, Typography } from "antd";
// import { saveAs } from "file-saver";
import { isMobile } from "react-device-detect";
import Container from "./Container";

const { Title } = Typography;

const Users = () => {
  // const downloadFile = (type: string) => {
  //   const fileUrl =
  //     type === "forKids"
  //       ? "src/assets/users/KIDSRUN.xlsx"
  //       : type === "5"
  //       ? "src/assets/users/5km.xlsx"
  //       : "src/assets/users/10km.xlsx";

  //   const fileName =
  //     type === "forKids"
  //       ? "500мKidsRun.xlsx"
  //       : type === "5"
  //       ? "Забег 5 km.xlsx"
  //       : "Забег 10 km.xlsx";

  //   saveAs(fileUrl, fileName);
  // };
  return (
    <Container>
      <Title
        style={{
          color: "#000",
          paddingBottom: isMobile ? "0px" : "30px",
          marginTop: isMobile ? "30px" : "50px",
        }}
      >
        Результаты FOREST RUN
      </Title>
      <Row gutter={[12, 24]} style={{ marginBottom: 20 }}>
        <Col span={isMobile ? 24 : 8}>
          <Button
            className="documents-button"
            icon={<TeamOutlined style={{ fontSize: "4rem" }} />}
            onClick={() =>
              window.open(
                "https://docs.google.com/spreadsheets/d/1G4jGjdpHAN3mzHGhl1zXPsHslll-u_5S/edit?usp=sharing&ouid=108049394068045741174&rtpof=true&sd=true"
              )
            }
          >
            <div className="documents">500м Kids Run</div>
          </Button>
        </Col>
        <Col span={isMobile ? 24 : 8}>
          <Button
            className="documents-button"
            icon={<TeamOutlined style={{ fontSize: "4rem" }} />}
            onClick={() =>
              window.open(
                "https://docs.google.com/spreadsheets/d/1KmVufuzMVTb-gGPS1J4hxyk6d0DNGARs/edit?usp=sharing&ouid=108049394068045741174&rtpof=true&sd=true"
              )
            }
          >
            <div className="documents">Забег 5 km</div>
          </Button>
        </Col>
        <Col span={isMobile ? 24 : 8}>
          <Button
            className="documents-button"
            icon={<TeamOutlined style={{ fontSize: "4rem" }} />}
            // onClick={() => downloadFile("10")}
            onClick={() =>
              window.open(
                "https://docs.google.com/spreadsheets/d/12jbilYnrJbP9jeXSlCggvZGKwxFmjyKl/edit?usp=sharing&ouid=108049394068045741174&rtpof=true&sd=true"
              )
            }
          >
            {" "}
            <div className="documents">Забег 10 km</div>
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default Users;
