import { Layout } from "antd";

const { Header } = Layout;

const HeaderLayout = () => {
  return (
    <Header>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          maxWidth: "1280px",
          margin: "auto",
        }}
      >
        <div style={{ width: "100px", height: "auto" }}>
          <img
            alt="Логотип"
            style={{
              width: "100px",
              height: "auto",
              backgroundImage: "url(src/assets/logo.png)",
            }}
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            paddingLeft: "5px",
          }}
        >
          <div className="header-items">О НАС</div>
          <div className="header-items">ВОЛОНТЕРСТВО</div>
          <div className="header-items">ДОКУМЕНТЫ</div>
          <div className="header-items">МАГАЗИН</div>
        </div>
      </div>
    </Header>
  );
};

export default HeaderLayout;
