import { Col, Image, Row, Space, Tabs, TabsProps, theme } from "antd";
import { useState } from "react";
import { isMobile } from "react-device-detect";
import Container from "./Container";
import { useAppSelector } from "../store/hooks";

const DistanceItem = ({ image, age, distance, entryFee, timeFinish }: any) => {
  return (
    <Row>
      <Col span={isMobile ? 24 : 12} order={isMobile ? 2 : 1}>
        <Image height={"100%"} src={image} />
      </Col>
      <Col span={isMobile ? 24 : 12} order={isMobile ? 1 : 2}>
        <div
          style={{
            padding: isMobile ? "15px 0 15px 20px" : "15px 15px",
            color: "#fff",
          }}
        >
          <Space size={20} style={{ paddingTop: 10 }} direction="vertical">
            <div style={{ fontSize: "1rem" }}>{distance}</div>
            <div style={{ display: "flex" }}>
              <div style={{ fontSize: "1.2rem", width: "200px" }}>
                Возраст:{" "}
              </div>
              <div style={{ fontSize: "1.2rem", fontWeight: 600 }}>{age}</div>
            </div>
            <div style={{ display: "flex" }}>
              <div style={{ fontSize: "1.2rem", width: "200px" }}>
                Расстояние:
              </div>
              <div style={{ fontSize: "1.2rem", fontWeight: 600 }}>
                {distance}
              </div>
            </div>
            <div style={{ display: "flex" }}>
              <div style={{ fontSize: "1.2rem", width: "200px" }}>
                Стартовый взнос:
              </div>
              <div style={{ fontSize: "1.2rem", fontWeight: 600 }}>
                {entryFee}
              </div>
            </div>
            <div style={{ display: "flex" }}>
              <div
                style={{
                  fontSize: "1.2rem",
                  minWidth: "200px",
                  width: "200px",
                }}
              >
                Лимит времени на прохождение дистанции:
              </div>
              <div
                style={{
                  fontSize: "1.1rem",
                  fontWeight: 600,
                  whiteSpace: "break-spaces",
                }}
              >
                {timeFinish}
              </div>
            </div>
          </Space>
        </div>
      </Col>
    </Row>
  );
};

const Distance = () => {
  const [activeKey, setActiveKey] = useState("1");
  const data = useAppSelector((state) => state.marathons.marathon);

  const onChange = (key: any) => {
    setActiveKey(key);
  };

  const items: TabsProps["items"] = data?.races?.map((el) => {
    return {
      key: el.key,
      label: el.distance,
      children: (
        <DistanceItem
          age={el?.age}
          distance={el?.distance}
          entryFee={el?.price}
          timeFinish={el?.duration}
          image={el?.image}
        />
      ),
    };
  });
  const { useToken } = theme;
  const { token } = useToken();

  return (
    <Container background={{ style: { backgroundColor: token.colorBgLayout } }}>
      <div className={`distance ${isMobile ? "isMobile" : ""}`}>
        <Tabs
          tabBarStyle={{ paddingTop: 10 }}
          tabPosition={isMobile ? "top" : "right"}
          style={{ color: "#fff" }}
          activeKey={activeKey}
          items={items}
          onChange={onChange}
        />
      </div>
    </Container>
  );
};

export default Distance;
