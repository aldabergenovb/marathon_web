import { FileTextOutlined } from "@ant-design/icons";
import { Button, Col, Row, Typography } from "antd";
import Container from "./Container";
import { isMobile } from "react-device-detect";
import { saveAs } from "file-saver";

const { Title } = Typography;

const Documents = () => { 
  const downloadFile = (type: string) => {
    const fileUrl =
      type === "position"
        ? "src/assets/doc3.pdf"
        : type === "raspiska"
        ? "src/assets/doc2.docx"
        : "src/assets/doc2.pdf";

    const fileName =
      type === "position"
        ? "Положение.pdf"
        : type === "raspiska"
        ? "Расписка.docx"
        : "Расписка для детей.pdf";

    saveAs(fileUrl, fileName);
  };
  return (
    <Container>
      <Title
        style={{
          color: "#000",
          paddingBottom: isMobile ? "0px" : "30px",
          marginTop: isMobile ? "30px" : "50px",
        }}
      >
        Наши документы
      </Title>
      <Row gutter={[12, 24]} style={{ marginBottom: 20 }}>
        <Col span={isMobile ? 24 : 8}>
          <Button
            className="documents-button"
            icon={<FileTextOutlined style={{ fontSize: "4rem" }} />}
            onClick={() => downloadFile("position")}
          >
            <div className="documents">Положение</div>
          </Button>
        </Col>
        <Col span={isMobile ? 24 : 8}>
          <Button
            className="documents-button"
            icon={<FileTextOutlined style={{ fontSize: "4rem" }} />}
            onClick={() => downloadFile("raspiska")}
          >
            <div className="documents">Расписка</div>
          </Button>
        </Col>
        <Col span={isMobile ? 24 : 8}>
          <Button
            className="documents-button"
            icon={<FileTextOutlined style={{ fontSize: "4rem" }} />}
            onClick={() => downloadFile("forKids")}
          >
            <div className="documents">Расписка для детей</div>
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default Documents;
