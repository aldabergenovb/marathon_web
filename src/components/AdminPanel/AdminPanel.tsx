import AxiosDigestAuth from "@mhoc/axios-digest-auth";
import { Button } from "antd";
import axios from "axios";
import { ProTabulator } from "pro-tabulator";
import { ProTabulatorColumn } from "pro-tabulator/dist/types";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const getTableRequest = (getLazyQuery: (params: any) => any) => {
  console.log(123);
  return async ({ current, pageSize, ...params }) => {
    const data = await getLazyQuery({
      ...params,
    } as any);

    return {
      data: data.data,
      total: data.records,
    };
  };
};

const formatDate = (dateString: string) => {
  if (!dateString) return "";
  const date = new Date(dateString);
  const day = date.getDate().toString().padStart(2, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const year = date.getFullYear();
  return `${day}.${month}.${year}`;
};

const AdminPanel = () => {
  const hasJwt = localStorage.getItem("token") ? true : false;
  const navigate = useNavigate();

  const [first, setfirst] = useState(true);

  const token = localStorage.getItem("token");
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };

  useEffect(() => {
    if (!hasJwt) {
      navigate("/");
    }
  }, [hasJwt]);
  const updateStatus = async ({ bodyData }) => {
    try {
      const url = "https://aqmola-marathon.kz/api/MarathonUsers/put";
      await axios.put(url, bodyData, { headers });
      setfirst((prev) => !prev);
    } catch (error) {
      console.error("Ошибка при выполнении Put-запроса:", error);
    }
  };
  const deleteUser = async (id) => {
    try {
      const url = `https://aqmola-marathon.kz/api/MarathonUsers/${id}`;
      await axios.delete(url, { headers });
      setfirst((prev) => !prev);
    } catch (error) {
      console.error("Ошибка при выполнении Put-запроса:", error);
    }
  };

  const digestAuth = new AxiosDigestAuth({
    username: "test",
    password: "test",
  });

  const axiosWithDigest = axios.create();
  axiosWithDigest.defaults.timeout = 5000; // Настройте таймаут запроса по своему усмотрению

  // Добавьте поддержку Digest Authentication

  const fetchData = async (params: any) => {
    try {
      const response: any = await digestAuth.request({
        headers,
        method: "GET",
        url: "https://aqmola-marathon.kz/api/MarathonUsers",
        params,
      });
      console.log(response.data);
      return response.data;
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const columns: ProTabulatorColumn<any>[] = [
    {
      title: "email",
      dataIndex: "email",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Имя",
      dataIndex: "firstName",
    },
    {
      title: "Фамилия",
      dataIndex: "lastName",
    },
    {
      title: "Пол",
      dataIndex: "genderName",
    },
    {
      title: "Дата рождения",
      dataIndex: "birthDate",
      render: (date: string) => formatDate(date),
    },
    {
      title: "Город",
      dataIndex: "cityName",
    },

    {
      title: "Номер",
      dataIndex: "phoneNumber",
      width: 200,
    },
    {
      title: "Экстренный контакт",
      dataIndex: "extraContact",
    },
    {
      title: "Дистанция",
      dataIndex: "distance",
    },
    {
      title: "Размер",
      dataIndex: "size",
    },
    {
      title: "Статус",
      dataIndex: "isPaid",
      render(value) {
        if (value) {
          return "Оплачено";
        } else {
          return "Не оплачено";
        }
      },
      excelRender(text) {
        if (text) {
          return "Оплачено";
        } else {
          return "Не оплачено";
        }
      },
    },
    {
      title: "Действие",
      dataIndex: "action",
      render(value, record: any) {
        return (
          <>
            <Button
              onClick={() => {
                updateStatus({ bodyData: { ...record, isPaid: true } });
                console.log(value);
              }}
              disabled={record.isPaid === true}
              style={{ marginBottom: 5 }}
            >
              Оплачен
            </Button>
            <Button
              onClick={() => {
                deleteUser(record.id);
                console.log(value);
              }}
              disabled={record.isPaid === true}
            >
              Удалить
            </Button>
          </>
        );
      },
      excelRender() {
        return "";
      },
    },
  ];

  return (
    <div
      style={{
        width: "100%",
        minHeight: "100vh",
        height: "100%",
        padding: 20,
        backgroundColor: "gray",
      }}
    >
      <div
        style={{
          width: "100%",
          padding: "10px",
          display: "flex",
          justifyContent: "end",
        }}
      >
        <Button
          onClick={() => {
            localStorage.setItem("token", "");
            navigate("/");
          }}
          style={{ marginLeft: "auto" }}
        >
          Выйти
        </Button>
      </div>
      <div
        style={{
          maxWidth: "1360px",
          margin: "auto",
          paddingTop: "100px",
        }}
      >
        {/* <Table columns={columns} dataSource={data} /> */}
        <ProTabulator<any, any>
          request={getTableRequest((params) => fetchData(params))}
          columns={columns as any}
          params={{ first }}
          disableHeightScroll
          pagination={false}
          id="ContractorsTable"
          rowKey="id"
          ordered
          editable
          editableProps={{
            hidden: {
              actions: true,
              create: true,
              manualCreate: true,
              deleteMultiple: true,
              saveMultiple: true,
            },
          }}
          downloadProps={{ fileName: "exsadas" }}
        />
      </div>
    </div>
  );
};

export default AdminPanel;
