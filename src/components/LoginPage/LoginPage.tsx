import { Button, Form, Input } from "antd";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const navigate = useNavigate();
  const handlePostRequest = async (values) => {
    try {
      const url = "https://aqmola-marathon.kz/api/Account/login";
      await axios.post(url, values).then((res) => {
        localStorage.setItem("token", res.data.data);
        navigate("/admin");
      });
    } catch (error) {
      console.error("Ошибка при выполнении POST-запроса:", error);
    }
  };
  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ width: 350 }}
        initialValues={{ remember: true }}
        onFinish={handlePostRequest}
        autoComplete="off"
      >
        <Form.Item label="Логин" name="login" rules={[{ required: true }]}>
          <Input />
        </Form.Item>

        <Form.Item label="Пароль" name="password" rules={[{ required: true }]}>
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Войти
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default LoginPage;
