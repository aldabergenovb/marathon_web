import { Button, Col, QRCode, Row } from "antd";

import imageAsDataURL from "../../assets/Kaspi_QR.svg"; // Замените путь к изображению на соответствующий путь в вашем проекте
import QRBlock from "../../assets/Kaspi_QR_header.svg"; // Замените путь к изображению на соответствующий путь в вашем проекте
import { isMobile } from "react-device-detect";

const Payments = ({ sum }: any) => {
  return (
    <Row gutter={32}>
      <Col span={isMobile ? 24 : 6} offset={3}>
        <div style={{ width: isMobile ? "100%" : "160px" }}>
          <img
            style={{ width: "140px", margin: "0 10px 20px" }}
            src={QRBlock}
          />
          {!isMobile ? (
            <QRCode
              value="https://pay.kaspi.kz/pay/d1thczfl?amount=100"
              icon={imageAsDataURL}
            />
          ) : (
            <Button
              style={{ maxWidth: "250px" }}
              href="https://pay.kaspi.kz/pay/d1thczfl?amount=100"
            >
              Оплатить
            </Button>
          )}
        </div>
      </Col>
      <Col span={isMobile ? 24 : 12}>
        <div
          style={{
            fontSize: "1.5rem",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div>Общая сумма:</div>
          <div style={{ fontWeight: 600 }}>{sum}тг</div>
        </div>
        <div style={{ fontWeight: 600, fontSize: "1rem" }}>
          <span style={{ fontSize: "1.5rem", color: "red" }}>Внимание </span>
          <br />
          При оплате укажите свои ФИО в 'Сообщение продавцу' и сохраните чек
        </div>
      </Col>
    </Row>
  );
};

export default Payments;
