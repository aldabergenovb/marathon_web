import { useAppSelector } from "../../store/hooks";

const ResultModal = () => {
  const data = useAppSelector((state) => state.marathons.marathon);
  return (
    <>
      <h1>Вы успешно зарегистровались на {data.title} </h1>
      <h2>В течении дня вам придет уведомление на почту </h2>
    </>
  );
};

export default ResultModal;
