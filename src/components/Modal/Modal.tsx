import { Button, Form, Modal, Steps } from "antd";
import axios from "axios";
import { useProFormCard } from "pro-template";
import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { setOpen } from "../../store/store";
import ModalForm from "./ModalForm";
import Payments from "./Payments";
import ResultModal from "./ResultModal";

const PaymentModal = () => {
  const data = useAppSelector((state) => state.marathons.marathon);
  const [form, actions] = useProFormCard({
    id: "modal_form",
    onUpdate: async () => {
      // await create(incident).unwrap();
    },
  });
  const distance = Form.useWatch("distance", form);
  useEffect(() => {
    form.setFieldValue(
      "price",
      data?.races?.find((el) => el.distance === distance)?.price
    );
  }, [distance]);
  const steps = [
    {
      key: "anketa",
      title: "АНКЕТА",
      content: <ModalForm form={form} actions={actions} />,
      ModalForm,
    },
    {
      key: "oplata",
      title: "ОПЛАТА",
      content: <Payments form={form} sum={form.getFieldValue("price")} />,
    },
    {
      key: "itog",
      title: "ИТОГ",
      content: <ResultModal />,
    },
  ];
  const dispatch = useAppDispatch();
  const open = useAppSelector((state) => state.marathons.open);
  const [current, setCurrent] = useState(0);
  const headers = {
    Accept: "application/json",
    // You can add other headers here if needed
  };

  const handlePostRequest = async () => {
    try {
      const url = "https://aqmola-marathon.kz/api/MarathonUsers/create";
      const bodyData = form.getFieldsValue();
      await axios.post(url, bodyData, { headers });
    } catch (error) {
      console.error("Ошибка при выполнении POST-запроса:", error);
    }
  };

  const items = steps.map((item) => ({ key: item.key, title: item.title }));
  return (
    <Modal 
      title="Регистрация"
      open={open}
      onCancel={() => dispatch(setOpen(false))}
      footer={[
        <Button
          type="primary"
          onClick={() => setCurrent((prev) => prev - 1)}
          disabled={current === 0}
        >
          Назад
        </Button>,
        <Button
          type="primary"
          onClick={async () => {
            if (current === 0) {
              await form.validateFields();
              const valid = await form.validateFields();
              if (valid) {
                await handlePostRequest().then(() => {
                  setCurrent((prev: number) => prev + 1);
                });
              }
            } else {
              setCurrent((prev: number) => prev + 1);
            }
          }}
          disabled={current === 2}
        >
          Далее
        </Button>,
      ]}
      width={isMobile ? "auto" : "60%"}
    >
      <Steps
        current={current}
        items={items}
        style={{
          marginTop: "20px",
          marginBottom: "30px",
          display: isMobile ? "none" : "",
        }}
      />
      <>{steps[current]?.content}</>
    </Modal>
  );
};

export default PaymentModal;
