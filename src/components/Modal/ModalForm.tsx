import { FormInstance } from "antd";
import { ProFormCard } from "pro-template";
import { useAppSelector } from "../../store/hooks";

const ModalForm = ({
  form,
  actions,
}: {
  form: FormInstance<Record<string, any>>;
  actions: any;
}) => {
  const data = useAppSelector((state) => state.marathons.marathon);

  return (
    <>
      <ProFormCard
        transparent
        form={form}
        actions={actions}
        span={12}
        fields={[
          { type: "submitter", hidden: true },
          {
            title: "Информация",
            type: "item",
            span: 24,
            children: [
              {
                type: "text",
                props: {
                  name: "email",
                  label: "E-mail",

                  rules: [{ required: true }, { type: "email" }],
                },
              },
              {
                type: "text",
                props: {
                  name: "firstName",
                  label: "Имя",

                  rules: [{ required: true }],
                },
              },
              {
                type: "text",
                props: {
                  name: "lastName",
                  label: "Фамилия",

                  rules: [{ required: true }],
                },
              },
              {
                type: "date",
                props: {
                  name: "birthDate",
                  label: "Дата рождения",

                  rules: [{ required: true }],
                },
              },
              {
                type: "select",
                props: {
                  name: "genderName",
                  label: "Пол",
                  options: [
                    { value: "Женский", label: "Женский" },
                    { value: "Мужской", label: "Мужской" },
                  ],
                  rules: [{ required: true }],
                },
              },
              {
                type: "select",
                props: {
                  name: "countryName",
                  label: "Страна",
                  options: [{ value: "kz", label: "Казахстан" }],
                  rules: [{ required: true }],
                },
              },
              {
                type: "select",
                props: {
                  name: "cityName",
                  label: "Город",
                  options: [
                    "Астана", // Столица
                    "Алматы", // Бывшая столица и крупнейший город
                    "Шымкент",
                    "Караганда",
                    "Тараз",
                    "Павлодар",
                    "Усть-Каменогорск",
                    "Кызылорда",
                    "Актау",
                    "Актобе",
                    "Семей",
                    "Атырау",
                    "Темиртау",
                    "Костанай",
                    "Петропавловск",
                    "Орал (Уральск)",
                    "Талдыкорган",
                  ],
                  rules: [{ required: true }],
                },
              },
            ],
          },
          {
            title: "Контакты",
            type: "item",
            span: 24,
            children: [
              {
                type: "number",

                props: {
                  name: "phoneNumber",
                  label: "Мобильный телефон",
                  fieldProps: { prefix: "+7", maxLength: 10, minLength: 10 },

                  rules: [{ required: true }],
                },
              },

              {
                type: "text",
                props: {
                  name: "extraContact",
                  label: "Экстренный контакт",

                  rules: [{ required: true }],
                },
              },
              // {
              //   type: "select",
              //   props: {
              //     name: "size",
              //     label: "Размер",
              //     options: ["XXS", "XS", "S", "M", "L", "XL", "XXL", "XXXL"],
              //     rules: [{ required: true }],
              //   },
              // },
              {
                type: "select",
                props: {
                  name: "distance",
                  options: data.races?.map((el) => el.distance),
                  label: "Дистанция",
                  rules: [{ required: true }],
                },
              },
              {
                type: "number",
                props: {
                  name: "price",

                  label: "Сумма",
                  disabled: true,
                  fieldProps: { prefix: "₸" },
                },
              },
            ],
          },
        ]}
      />
    </>
  );
};

export default ModalForm;
