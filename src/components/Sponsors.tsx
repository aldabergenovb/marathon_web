import { Col, Row, Typography } from "antd";
import Container from "./Container";
import { isMobile } from "react-device-detect";

const { Title } = Typography;
const Sponsors = () => {
  return (
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    <Container className={`sponsors ${isMobile ? "isMobile" : ""}`}>
      <Title
        style={{
          color: "#000",
          paddingBottom: 30,
          paddingTop: 30,
        }}
      >
        Наши партнеры
      </Title>
      <Row
        gutter={isMobile ? [32, 32] : [32, 32]}
        style={{ paddingBottom: 50 }}
      >
        <Col span={isMobile ? 24 : 6}>
          <div
            className="sponsor-image"
            style={{
              backgroundImage: "url(src/assets/gempire-logo.jpg)",
              maxWidth: isMobile ? "auto" : "100%",
            }}
          ></div>
        </Col>
        <Col span={isMobile ? 24 : 6}>
          <div
            className="sponsor-image"
            style={{
              backgroundImage: "url(src/assets/fitnes-logo.png)",
              maxWidth: isMobile ? "auto" : "100%",
            }}
          ></div>
        </Col>
        <Col span={isMobile ? 24 : 6}>
          <div
            className="sponsor-image"
            style={{
              backgroundImage: "url(src/assets/aqmola-logo.jpg)",
              maxWidth: isMobile ? "auto" : "100%",
            }}
          ></div>
        </Col>
        <Col span={isMobile ? 24 : 6}>
          <div
            className="sponsor-image"
            style={{
              backgroundImage: "url(src/assets/Greenstudio_logo.png",
              maxWidth: isMobile ? "auto" : "100%",
            }}
          ></div>
        </Col>
        <Col span={isMobile ? 24 : 6}>
          <div
            className="sponsor-image"
            style={{
              backgroundImage: "url(src/assets/jeti_kazyna_logo.JPG",
              maxWidth: isMobile ? "auto" : "100%",
            }}
          ></div>
        </Col>
        <Col span={isMobile ? 24 : 6}>
          <div
            className="sponsor-image"
            style={{
              backgroundImage: "url(src/assets/mystart.png",
              maxWidth: isMobile ? "auto" : "100%",
            }}
          ></div>
        </Col>
      </Row>
    </Container>
  );
};

export default Sponsors;
