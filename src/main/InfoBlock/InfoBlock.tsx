import { Divider, Space, Typography } from "antd";
import Container from "../../components/Container";
import { useAppSelector } from "../../store/hooks";
import { isMobile } from "react-device-detect";
const { Title } = Typography;
const InfoBlock = () => {
  const data = useAppSelector((state) => state.marathons.marathon);
  return (
    <Container>
      <div
        style={{
          padding: 20,
          fontSize: "1.3rem",
          marginRight: "auto",
          marginLeft: "auto",
          lineHeight: 1.5,
        }}
      >
        <Space
          style={{ width: "100%", overflow: "hidden" }}
          direction="vertical"
          size={10}
        >
          {data?.subTitle && (
            <>
              <Divider orientation="left" orientationMargin="0">
                <Title
                  style={{
                    textAlign: "center",
                    fontSize: "1.5rem",
                  }}
                >
                  <Space>
                    О МЕРОПРИЯТИИ
                  </Space>
                </Title>
              </Divider>
              <p style={{ fontSize: "1.1rem", textAlign: "justify" }}>
                {data?.about ? data?.about : data?.subTitle}
              </p>
            </>
          )}
          <Divider orientation="left" orientationMargin="0">
            <Title
              style={{
                textAlign: "center",
                fontSize: "1.5rem",
              }}
            >
              <Space>
            О НАС
              </Space>
            </Title>
          </Divider>

          <p style={{ fontSize: "1.1rem", textAlign: "justify" }}>
            Мы, команда энтузиастов, в течение года проводим серию пробегов,
            входящих в беговое событие «AQMOLA MARATHON». Трассы беговых
            маршрутов проходят по Акмолинской области, охватывая невысокие холмы
            юго- западного мелкосопочника, знаменитую Тенгиз- Коргалжынскую
            низменность на юге области, сосново-березовые леса и горы
            Бурабайского национального парка на севере.
            <br /> В беговых событиях принимают участие любители бега всех
            возрастов и уровней подготовки, объединенных общей страстью к
            массовому спорту и здоровому образу жизни. Для всех участников – это
            возможность не только укрепить свое тело и дух, но и лучше узнать
            природу и историю родного края.
          </p>
          <Divider orientation="left" orientationMargin="0">
            <Title
              style={{
                textAlign: "center",
                fontSize: "1.5rem",
              }}
            >
              <Space>
                НАША МИССИЯ
              </Space>
            </Title>
          </Divider>
          <p style={{ fontSize: "1.1rem", textAlign: "justify" }}>
            Мы верим в то, что любительский массовый спорт и здоровый образ
            жизни способны изменить жизни людей к лучшему, укрепляя и повышая
            здоровье нации, его долголетие через личное благополучие каждого.
            <br />
            Наша миссия – это вдохновение и мотивация жителей и гостей
            Казахстана на достижение своих целей через участие в пробегах
            «AQMOLA MARATHON».
            <br />
            Мы стремимся создать привлекательную и безопасную среду, где каждый
            участник испытает радость движения и преодоления себя.
          </p>
          <Divider orientation="left" orientationMargin="0">
            <Title
              style={{
                textAlign: "center",
                fontSize: "1.3rem",
              }}
            >
              <Space style={{ fontSize: isMobile ? "1rem" : "" }}>
                СОЦИАЛЬНАЯ ОТВЕТСТВЕННОСТЬ
              </Space>
            </Title>
          </Divider>
          <p style={{ fontSize: "1.1rem", textAlign: "justify" }}>
            Мы участвуем в социально значимых проектах и благотворительных
            мероприятиях по Акмолинской области, используя беговые события
            «AQMOLA MARATHON», как средство содействия и влияния позитивным
            изменениям в нашем сообществе.
          </p>
        </Space>
      </div>
    </Container>
  );
};

export default InfoBlock;
