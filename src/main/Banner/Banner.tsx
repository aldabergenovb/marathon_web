import { Carousel, Grid, Space } from "antd";
import { CarouselRef } from "antd/es/carousel";
import { useEffect, useRef } from "react";
import Container from "../../components/Container";
import { marathons } from "../../data/marathons";
import { useAppSelector } from "../../store/hooks";
import "./Banner.scss";
import BannerContent from "./components/BannerContent";
import banner from "./../../assets/banner/IMG_0895.jpg";
import boro from "./../../assets/banner/boro.jpg";
import koksh from "./../../assets/banner/koksh.jpeg";
import { InstagramOutlined, PhoneOutlined } from "@ant-design/icons";
const { useBreakpoint } = Grid;
const Banner = () => {
  const data = useAppSelector((state) => state.marathons.marathon);
  const carouselRef = useRef<CarouselRef>(null);
  const { xl } = useBreakpoint();
  useEffect(() => {
    if (data) {
      carouselRef.current?.goTo(marathons?.findIndex((m) => m === data));
    } else {
      carouselRef.current?.goTo(2);
    }
  }, [data]);

  return (
    <Container
      isStatic
      background={{
        className: "banner_container",
        style: {
          background: `url(${
            data?.title === "FOREST RUN"
              ? boro
              : data?.title === "ISHIM HALF MARATHON"
              ? koksh
              : banner
          })`,
        },
      }}
    >
      <Carousel
        ref={carouselRef}
        dots={false}
        effect="fade"
        style={{ height: "100%" }}
      >
        {marathons.map((marathon, index) => (
          <BannerContent key={index} marathon={marathon} />
        ))}
        <div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              color: "#fff",
              padding: xl ? "" : "20px",
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "flex-start",
                justifyContent: "space-between",
              }}
            >
              <div
                className="logo"
                style={{
                  height: xl ? "200px" : "100px",
                  width: xl ? "450px" : "250px",
                  backgroundImage: `url(src/assets/logo.png)`,
                }}
              ></div>
              <Space
                size={xl ? 24 : 6}
                direction={xl ? "horizontal" : "vertical"}
                style={{ paddingLeft: 8 }}
              >
                <Space
                  onClick={() =>
                    window.open(
                      "https://www.instagram.com/aqmola_marathon/?igshid=MzRlODBiNWFlZA%3D%3D",
                      "_blank"
                    )
                  }
                  style={{ color: "#fff", fontSize: xl ? "20px" : "18px" }}
                >
                  <InstagramOutlined />
                  aqmola_marathon
                </Space>
                <Space
                  onClick={() => (window.location.href = `tel:+7474555206`)}
                  style={{ color: "#fff", fontSize: xl ? "20px" : "18px" }}
                >
                  <PhoneOutlined />
                  +77474555206
                </Space>
              </Space>
            </div>

            <div
              style={{
                marginTop: xl ? "" : "50px",
                fontSize: xl ? "3rem" : "1.8rem",
              }}
            >
              Жеңіске бірге жетеміз!
            </div>
          </div>
          <div
            style={{
              fontSize: xl ? "2rem" : "1.2rem",
              color: "#fff",
              padding: xl ? "" : "20px",
            }}
          >
            Проводится при поддержке акимата Акмолинской области
          </div>
        </div>
      </Carousel>
    </Container>
  );
};

export default Banner;
