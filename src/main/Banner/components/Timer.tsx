import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";

const Timer = ({ date }: { date: Date }) => {
  const [timeRemaining, setTimeRemaining] = useState<any>({});
  useEffect(() => {
    const countdownDate = new Date(date).getTime();

    const updateTimer = setInterval(() => {
      const now = new Date().getTime();
      const distance = countdownDate - now;

      if (distance < 0) {
        clearInterval(updateTimer);
        setTimeRemaining({ days: 0, hours: 0, minutes: 0, seconds: 0 });
      } else {
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);
        setTimeRemaining({ days, hours, minutes, seconds });
      }
    }, 1000);

    return () => {
      clearInterval(updateTimer);
    };
  }, [date]);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: isMobile ? "space-around" : "space-between",
      }}
    >
      <div className={`countdown `}>
        <div className="countdown_count">{timeRemaining.days ?? 0}</div>
        <div className="countdown_text">Д</div>
      </div>
      <div className={`countdown`}>
        <div className="countdown_count">{timeRemaining.hours ?? 0}</div>
        <div className="countdown_text">Ч</div>
      </div>
      <div className={`countdown`}>
        <div className="countdown_count">{timeRemaining.minutes ?? 0}</div>
        <div className="countdown_text">М</div>
      </div>
      <div className={`countdown`}>
        <div className="countdown_count">{timeRemaining.seconds ?? 0}</div>
        <div className="countdown_text">С</div>
      </div>
    </div>
  );
};

export default Timer;
