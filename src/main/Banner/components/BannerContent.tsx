import { Grid, Typography } from "antd";
import { MarathonsType } from "../../../data/types";
import Timer from "./Timer";
import Header from "../../Header/Header";
const { Title, Paragraph } = Typography;
const { useBreakpoint } = Grid;

const BannerContent = ({ marathon }: { marathon?: MarathonsType }) => {
  const { xl } = useBreakpoint();
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        color: "#fff",
        flexDirection: "column",
        padding: 20,
        paddingTop: 40,
        height: "100%",
      }}
    >
      <>
        {xl && <Header />}
        <div>
          <Title
            style={{
              color: "#fff",
              fontSize: xl ? "6rem" : "3rem",
              marginBottom: 0,
            }}
          >
            {marathon.title}
          </Title>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              flexDirection: xl ? "row" : "column",
              alignItems: "center",
            }}
          >
            <Title
              style={{
                color: "#fff",
                fontSize: xl ? "3rem" : "1.7rem",
                marginTop: 10,
                width: xl ? "70%" : "100%",
              }}
            >
              {marathon.subTitle}
            </Title>
            
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Paragraph
            style={{
              color: "#fff",
              fontSize: xl ? "3rem" : "2rem",
              margin: "auto",
              marginLeft: 0,
            }}
          >
            {marathon.dateText}
          </Paragraph>
          {xl && <Timer date={marathon.date} />}
        </div>
      </>
    </div>
  );
};

export default BannerContent;
