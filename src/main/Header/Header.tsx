import { Grid, Space } from "antd";
import Container from "../../components/Container";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { selectMarathon } from "../../store/store";
import { InstagramOutlined } from "@ant-design/icons";
const { useBreakpoint } = Grid;

const Header = () => {
  const data = useAppSelector((state) => state.marathons.marathon);
  const dispatch = useAppDispatch();
  const { xl } = useBreakpoint();
  return (
    <Container>
      <div
        className="logo-container"
        style={{
          height: data ? 70 : 0,
          overflow: "hidden",
          transition: " all 0.2s linear",
          display: "flex",
          alignItems: "center",
        }}
      >
        <div
          style={{
            backgroundImage: "url(src/assets/logo.png)",
            backgroundRepeat: "no-repeat",
            backgroundSize: "contain",
            height: "65px",
            width: "170px",
            cursor: "pointer",
          }}
          onClick={() => dispatch(selectMarathon(undefined))}
        ></div>
        <div
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            fontSize: xl ? "2rem" : "2rem",
            letterSpacing: "5px",
            fontWeight: 600,
            WebkitBackgroundClip: "text",
            color: "#fff",
          }}
          className="logo-text"
        >
          Жеңіске бірге жетеміз!
        </div>
        <Space
          onClick={() =>
            window.open(
              "https://www.instagram.com/aqmola_marathon/?igshid=MzRlODBiNWFlZA%3D%3D",
              "_blank"
            )
          }
          style={{ color: "#fff", fontSize: "23px" }}
        >
          <InstagramOutlined />
          aqmola_marathon
        </Space>
      </div>
    </Container>
  );
};

export default Header;
