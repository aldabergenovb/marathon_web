import { theme } from "antd";
import Distance from "../components/Distance";
import Documents from "../components/Documents";
import Footer from "../components/Footer";
import MarathonsList from "../components/MarathonsList";
import PaymentModal from "../components/Modal/Modal";
import Sponsors from "../components/Sponsors";
import { useAppSelector } from "../store/hooks";
import Banner from "./Banner/Banner";
import InfoBlock from "./InfoBlock/InfoBlock";
import Users from "../components/Users";

const Main = () => {
  const data = useAppSelector((state) => state.marathons.marathon);
  const { useToken } = theme;
  const { token } = useToken();
  return (
    <div>
      <Banner />
      <MarathonsList />
      <InfoBlock />
      <Distance />
      <Users />
      <Sponsors />
      <div
        style={{
          height: "80px",
          width: "100%",
          backgroundColor: data?.style?.color ?? token.colorBgLayout,
        }}
      ></div>
      {/* <Subscribe /> */}
      <Documents />
      <Footer />
      <PaymentModal />
      {/* <AdminPanel /> */}
    </div>
  );
};

export default Main;
